# Building Instruction

## 1) OS/Software install and configuration

- **Don't upgrade to 16.04**. 
- You must have an Ethernet connection to do the following (until you install Wifi driver). 

##### Grinch Kernel 
- Install Grinch kernel: see http://www.jetsonhacks.com/2015/05/26/install-grinch-kernel-for-l4t-21-3-on-nvidia-jetson-tk1/
```
sudo apt-get install git
git clone https://github.com/jetsonhacks/installGrinch.git
cd installGrinch
./installGrinch.sh
```
- Also, download Grinch kernel sources
    ```
    wget http://www.jarzebski.pl/files/jetsontk1/grinch-21.3.4/jetson-tk1-grinch-21.3.4-source.tar.bz2
    sudo tar -C /usr/src -vxjf jetson-tk1-grinch-21.3.4-source.tar.bz2
    ```
- Then, reboot TK1.

##### Wifi adapter driver
```
sudo apt-get install linux-headers-generic build-essential git
git clone https://github.com/diederikdehaas/rtl8812AU.git  
cd rtl8812AU 
make ARCH=arm
sudo make install
sudo modprobe 8812au
```
+ **Note**: You will be able to SSH into your TK1 through Picostation later. But you can also do that through its wifi interface if you know its IP. This wifi is mainly for connecting your TK1 to the Internet. 


##### ROS indigo

- Follow the direction in http://wiki.ros.org/indigo/Installation/Ubuntu
- Basically you will need to do the following (after configuring the repositories to allow "restricted," "universe," and "multiverse."):
    ```
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
    sudo apt-get update
    sudo apt-get install ros-indigo-desktop
    sudo rosdep init
    rosdep update
    echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
    source ~/.bashrc
    sudo apt-get install python-rosinstall
    ```
- Known issues
   + can't install ros-indigo-desktop-full
   + rviz will not work. (Updated 10/16/2017: see http://www.jetsonhacks.com/2016/09/08/ros-rviz-nvidia-jetson-tk1/)

- Once ROS is installed, follow these ROS tutorials ([ROS tutorial1](F1_10_manuals/tutorials/t1.pdf), [ROS tutorial2](F1_10_manuals/tutorials/t2.pdf)) from F1/10. See also http://wiki.ros.org/ROS/Tutorials

##### PyCuda
```
wget https://pypi.python.org/packages/b3/30/9e1c0a4c10e90b4c59ca7aa3c518e96f37aabcac73ffe6b5d9658f6ef843/pycuda-2017.1.1.tar.gz#md5=9e509f53a23e062b31049eb8220b2e3d
tar xzvf
cd pycuda-2017.1.1
./configure.py --cuda-root=/usr/local/cuda 
sudo make install
```

## 2) 'Real' Hardware assemby

For the hardware assembly, refer to [manual1](F1_10_manuals/BuildInstructions.pdf) and [manual2](F1_10_manuals/TheBuild.pdf). 

For laser cutting
- You must 1) have CEID membership and 2) have completed the laser cutter training. 
- Use plywood. Do not use ABS as it is not allowed by CEID. 

![](https://github.com/CertiKOS/selfdrivingcar/blob/master/platform_building/Lasercut.JPG)


## 3) Sensor and peripherals configuration
- Teensy firmware: follow the direction [here](https://github.com/mlab-upenn/f1tenthpublic/tree/master/code/Teensy%20Firmware). Do this on your TK1, not on desktop. The firmware is [here](https://github.com/CertiKOS/selfdrivingcar/tree/master/code/Teensy). 
    - Can't see /dev/ttyACMx? It is likely that your Teensy has been never been programmed. See [this](https://www.pjrc.com/teensy/troubleshoot.html). 
- IMU firmware
    + [Sparkfun 9DoF Razor IMU M0 Hookup Guide](https://learn.sparkfun.com/tutorials/9dof-razor-imu-m0-hookup-guide?_ga=2.116753606.1788712538.1507398257-1231950949.1503425084)
        * Setup Arduino as explained there
    + [razor_imu_9dof ROS package](http://wiki.ros.org/razor_imu_9dof)
        * (As of Oct 2017) The 'official' firmware doesn't support Sparkfun 9DoF Razor IMU M0. 
        * Instead, install this modified one -- https://github.com/CertiKOS/selfdrivingcar/tree/master/code/IMU (modified from https://github.com/Murphy3220/Razor-AHRS-SEN-14001)
        



- Picostation
    + Never turn on airMAX.
    + Go Edit Connections->IPv4 Settings->Routes->Check 'Use this connection only for resources on its network'
- LIDAR
    + See [this](https://github.com/CertiKOS/selfdrivingcar/tree/master/tasks#task2)
    + If using Hokuyo (which connects via Ethernet), a bridge should be set up. 
- USB cam
- Xbox360 wireless controller
    + If the ros joystick driver has not been installed, install it:
    ```
    sudo apt-get install ros-indigo-joystick-drivers
    ```
    + You have to run `joy_node` whenever you want to use the xbox controller: (Don't forget to run `roscore` first)
    ```
    rosrun joy joy_node
    ```
    + You can check if the controller is working by echoing `/joy` topic:
    ```
    rostopic echo /joy
    ```
