from https://github.com/DaikiMaekawa/hector_slam_example

with some changes for Y driving car.

## Install the dependency packages
copy this package into ~/catkin_ws/src 
```sh
$ rosdep install hector_slam_example
```

## Usage

#### openni

```sh
$ roslaunch hector_slam_example hector_openni.launch
```    

#### UTM-30LX

```sh
$ roslaunch hector_slam_example hector_hokuyo.launch
```

#### UST-10LX/20LX
the lidar on Car2 is HOKUYO-UST-10LX，and you should remind the ip-addr of lidar in this launch file
```sh
$ roslaunch hector_slam_example hector_hokuyo_eth.launch
```

[![rviz](http://img.youtube.com/vi/xo64T0jgKKQ/0.jpg)](https://www.youtube.com/watch?v=xo64T0jgKKQ)

## License

Copyright (c) 2014, [Daiki Maekawa](http://daikimaekawa.strikingly.com/). (MIT License)

See LICENSE for more info.
