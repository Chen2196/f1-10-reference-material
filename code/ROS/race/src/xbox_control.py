import rospy
from race.msg import drive_param
from sensor_msgs.msg import Joy

VELOCITY_SCALE = 50
ANGLE_SCALE = 50
MOTORSTART = 0.5

def xbox_control(data):
	msg = drive_param()
	if (data.axes[1] > 0 && data.axes[1] < MOTORSTART)
		data.axes[1] = MOTORSTART
	else if  (data.axes[1] < 0 && data.axes[1] > -MOTORSTART)
		data.axes[1] = -MOTORSTART

	msg.velocity = VELOCITY_SCALE*data.axes[1] # left stick up/down
	msg.angle = -1*ANGLE_SCALE*data.axes[0] # left stick left/right

	print('Velocity:', msg.velocity)
	print('Angle:', msg.angle)
	pub.publish(msg)

pub = rospy.Publisher('drive_parameters', drive_param, queue_size=10)
rospy.Subscriber("/joy", Joy, xbox_control)
rospy.init_node('xbox_control', anonymous=True)
rospy.spin()
