## race 
a simple package that controls the vehicle by keyboard  

**Prerequisite:** Make sure ROS is installed on your TK1. Try `ls /opt/ros/indigo`. If not installed, see [this](https://github.com/CertiKOS/selfdrivingcar/tree/master/platform_building#ros-indigo).  

**Prerequisite:** Also, read [this](https://github.com/CertiKOS/selfdrivingcar/blob/master/platform_building/F1_10_manuals/tutorials/keyboard_control.pdf) first! What is [PWM](https://www.arduino.cc/en/Tutorial/PWM)?   

**Steps**  
- Suppose you haven't created a catkin workspace yet. Then, do the following:  
    ```  
    source /opt/ros/indigo/setup.bash
    mkdir -p ~/catkin_ws/src
    cd ~/catkin_ws
    catkin_make
    echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    source ~/.bashrc
    ```  
- Copy the `race` directory into your catkin workspace
    ```
    cp -r ~/selfdrivingcar/code/ROS/race/ ~/catkin_ws/src/
    ```
- Make
    ```
    cd ~/catkin_ws
    catkin_make
    ```
- Change the permissions of the python files so that they are executable
    ```
    chmod +x src/race/src/keyboard.py src/race/src/talker.py
    ```
- Run roscore first
    ```
    roscore
    ```
- Install `rosserial` (just once, and it is highly likely that these are installed already)
    ```
    sudo apt-get install ros-indigo-rosserial-arduino
    sudo apt-get install ros-indigo-rosserial
    ```
- And (just once), 
    ```
    source ~/.bashrc
    ```
- Then, run the others
    ```
    rosrun rosserial_python serial_node.py /dev/ttyACM0
    rosrun race talker.py
    rosrun race keyboard.py
    ```
    
**Notes**  
- /dev/ttyACM1 could be the Teensy's port. 
- If you see "[Errno 13] Permission denied: '/dev/ttyACM0'", do 
    ```
    sudo adduser [YOUR_USER_NAME] dialout
    ```
    and then re-login. 

- Did you power on the ESC (battery and button)? 

- If you haven't done, you may want to do this. Otherwise, you will need to source setup.bash everytime. 
    ```
    echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    source ~/.bashrc
    ```

---

## Publishing ROS topic to remote machines  
You can publish ros topics to remote machines. This is particularly useful for data visualization using rviz (as it doesn't run on our TK1). 
1. Add these to `.bashrc` in your TK1 (replace `172.28.229.223` with the actual IP address of TK1).
    ```
    export ROS_MASTER_URI=http://172.28.229.223:11311
    export ROS_HOSTNAME=172.28.229.223
    ```
2. Then, add the following line to `.bashrc` in your laptop/desktop (maybe the virtual machine running on it) where rviz works fine:
    ```
    export ROS_MASTER_URI=http://172.28.229.223:11311
    ```
3. **Don't forget to do `source .bashrc` or re-login.**

4. Now, on your laptop/desktop (again, the virtual machine), see what ROS topics are available by doing 
    ```
    rostopic list
    ```
    These topics are forwarded from the TK1. You can also run an ROS node that subscribes any of the topics.  