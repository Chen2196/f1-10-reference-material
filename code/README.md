### mobile control
** Drive the car by the xbox controller **: 
- Try and understand the base `race` package
- Xbox360 wireless controller
    + If ROS joystick driver has not been installed, install it:
    ```
    sudo apt-get install ros-indigo-joystick-drivers
    ```
    + You have to run `joy_node` whenever you want to use the xbox controller: (Don't forget to run `roscore` first)
    ```
    rosrun joy joy_node
    ```
    + You can check if the controller is working by echoing `/joy` topic:
    ```
    rostopic echo /joy
    ```
- Create and implement `xbox_con.py` that controls the car by the xbox controller. 
---

### lidar

#### 0. Goal 
Use LIDAR to avoid obstacles. When there is an obstacle in front of the car, it should not move toward the direction even if the xbox controller drives it toward that direction. 

##### Notes 

- **There is no right solution**. You may need to modify `talker.py` (and/or maybe `xbox_con.py`?), add a new node, create new topics, etc. 

- You may not want to do a sudden stop. As if you drive a real car, do a smooth stop. You may want to have a bound on the distance. For example, reduce velocity by 25% when the distance to an obstacle is X. Reduce by 50% when the car is closer to the obstacle. Make a full stop when the distance is some Y. Try different values of X and Y. This is a simple example and there might be a better approach. 

- If you use a monitor, you will see Ubuntu screen of 'ubuntu' account. Logout and then login with your account. 

#### 1. LIDAR launch

The two cars have different LIDARs. 

###### RPLIDAR (Black color)

+ Clone https://github.com/robopeak/rplidar_ros.git to your catkin workspace's src folder (e.g., `~/catkin_ws/src`) 
+ Make 
```
cd ~/catkin_ws
catkin_make
```
+ Run rplidar node
```
roslaunch rplidar_ros rplidar.launch
```
 + Then scan data is published as `/scan` topic. Try
```
rostopic echo /scan
```
+ See [this page](https://github.com/robopeak/rplidar_ros/wiki) for more information.

###### Hokuyo (Orange  color)

+ Install the `urg-node` package
```
sudo apt-get install ros-indigo-urg-node
```
+ Run `urg_node`
```
rosrun urg_node urg_node _ip_address:=192.168.1.11
```
+ Then scan data is published as `/scan` topic. Try
```
rostopic echo /scan
```
+ See [this page](https://www.hokuyo-aut.jp/search/single.php?serial=167) for more information. 


#### 2. LIDAR scan visualization
You can visualize LIDAR scan data on ROS rviz which unfortunately doesn't work on our TK1. But we can forward the scan data to a remote machine on which you can run rviz (and any other nodes, as if they run on the same machine). The most convenient way is to run ROS on your laptop, using VirtualBox (you should have received my email regarding this). **Note that this visualization is just to help you understand what LIDAR is and how it works.** When doing task 2, you don't need the visualization. 

- Once ROS is installed, you can follow [this](https://github.com/CertiKOS/selfdrivingcar/tree/master/code/ROS#publishing-ros-topic-to-remote-machines) to set up data forwarding.

- First, download [`rplidar.rviz`](rplidar.rviz) and save it on your machine (e.g., your home directory.)

- Then, on your machine, launch rviz (assuming you are using RPLIDAR. If you use Hokuyo, talk to Man-Ki):
    ```
    rosrun rviz rviz -d ~/rplidar.rviz
    ```
---

### IMU

#### 0. Goal 
Learning about IMU (Inertial Measurement Unit). What is it? 
- https://en.wikipedia.org/wiki/Inertial_measurement_unit
- http://www.starlino.com/imu_guide.html
What can you do with IMU? Can you improve task2 by incorporating IMU? 

#### 1. IMU Launching

- Install ROS razor_imu_9dof package
```
sudo apt-get install python-visual

sudo apt-get install ros-indigo-razor-imu-9dof

roscd razor_imu_9dof
cd config
sudo cp razor.yaml my_razor.yaml
sudo vi my_razor.yaml
```
Then change port to `/dev/ttyACM1`

- For 3D visualization, 
```
roslaunch razor_imu_9dof razor-pub-and-display.launch
```
![](https://github.com/CertiKOS/selfdrivingcar/blob/master/images/IMU_visualization.png)

- For publishing IMU data without visualization, 
```
roslaunch razor_imu_9dof razor-pub.launch
```
You should be able to see `\imu` topic (see the definition: http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html). 


---


### Camera

#### 0. Goal 
Play with USB camera and [opencv](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html). And collect a training data for vision-based learning. 

#### 1. Steps

- Install relevant ROS packages
```
sudo apt-get install ros-indigo-cv-camera
sudo apt-get install ros-indigo-image-view
```

- Launch [`cv_camera`](http://wiki.ros.org/cv_camera) ROS node
```
rosrun cv_camera cv_camera_node
```
- The image is published as `/cv_camera/image_raw` topic. You can view the image by [`image_view`](http://wiki.ros.org/image_view) node
```
rosrun image_view image_view image:=/cv_camera/image_raw    
```

#### 2. Requirements

- Create a ROS node that
    + captures USB camera image
    + captures your xbox controller input (velocity and angle)
    + shows the captured camera image
    + draws an arrow that indicates the current velocity and angle input from the xbox controller
- That is, the viewer should show the direction and velocity of the car superimposed on the USB camera image. Save the superimposed images to files so that we can create a video from them. 

- Create a log of the images (original images, not superimposed ones) and the xbox controller inputs
    + You should log timestamps as well. We will give these data to our machine learning algorithm. What is the best way to store training data? 

#### 3. Tips
- You should import `cv2` and `CvBridge`
    ```python
    import cv2
    from cv_bridge import CvBridge, CvBridgeError
    ```
- The image data you get from `cv_camera` is [sensor_msgs/Image](http://docs.ros.org/api/sensor_msgs/html/msg/Image.html) type. You shoud convert it to what opencv can process. 
    ```python
    bridge = CvBridge()
    ...
    def callback(ros_img_data):
        ...
        cv_image = bridge.imgmsg_to_cv2(ros_img_data, "bgr8")
        ...
    ```
- To display a cv image, 
    ```python
    cv2.imshow('any title', cv_image)
    cv2.waitKey(1)
    ```
- To convert cv image to ROS image
    ```python
    new_ros_image = bridge.cv2_to_imgmsg(cv_image, "bgr8")
    ```
    You can then publish this ROS image and display it on `image_view` node.
- You may need to set some parameters before running `cv_camera` and/or `image_view`. See [`cv_camera`](http://wiki.ros.org/cv_camera) and [`image_view`](http://wiki.ros.org/image_view)
- How often images are captured? How long does it take to capture the image, process, and save it to file? Is your code fast enough? 

##### 4.something new
Processing image message received from a camera node might cause a timing irregularity. Hence, the following method (i.e., retrieving image frame directly from the camera) should be better in terms of the timing regularity and latency. See also https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html
```python
import cv2
...
FPS = 10    # 10 frames/second
deviceID = 0    #the id of the opened video capturing device (i.e. a camera index).  
is_recording = False    # set/unset by xbox controller's buttons
...
rate = rospy.Rate(FPS)   
capture = cv2.VideoCapture(deviceID)  
...
while not rospy.is_shutdown():
    if is_recording:
        ret, frame = capture.read()
        ...
        #Process the frame
        #Save to file
        ...

    rate.sleep()
...
capture.release()
```
