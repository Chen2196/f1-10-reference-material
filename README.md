# README #

### F1/10 自动驾驶小车平台搭建及部分代码###
整体架构参考
- http://f1tenth.org/index

### 主要内容 ###
- **小车平台** platform building 包括小车平台机械结构及硬件结构
- **软件部分** code
	- ROS slam算法，xbox遥控，相机采集数据等
    - Tenssy board 底层电机及舵机控制，包括使用串口和ROS交互
    - IMU Razor9轴IMU，姿态解算

### Some references
- https://devblogs.nvidia.com/parallelforall/deep-learning-self-driving-cars/
- https://devblogs.nvidia.com/parallelforall/deep-learning-automated-driving-matlab/
- http://robotcar-dataset.robots.ox.ac.uk/
- http://neuralnetworksanddeeplearning.com/
- https://adeshpande3.github.io/adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks/
- http://colah.github.io/posts/2014-07-Conv-Nets-Modular/ (very easy to understand CNN)
- http://www.bzarg.com/p/how-a-kalman-filter-works-in-pictures/ (Kalman filter)
- http://robotics.itee.uq.edu.au/~elec3004/2015/tutes/Kalman_and_Bayesian_Filters_in_Python.pdf
- https://devblogs.nvidia.com/parallelforall/even-easier-introduction-cuda/ (nvidia CUDA)
